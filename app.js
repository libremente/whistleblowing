/***
This file is part of WhistleBlowing.

WhistleBlowing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

WhistleBlowing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with WhistleBlowing. If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 2015 Venis Spa

Authors: Venis <riuso@venis.it>
***/

'use strict';

var _ = require('underscore');
var nconf = require('nconf');
var selectn = require('selectn');
var express = require('express');
var helmet = require('helmet');
var exphbs = require('express-handlebars');
var formidable = require('express-formidable');
var session = require('express-session');
var flash = require('connect-flash');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var Promise = require('bluebird');
var moment = require('moment');
var fs = require('fs');
var rateLimit = require('express-rate-limit');
var sanitize = require('mongo-sanitize');
var email = require('emailjs');

var Utils = require('./utils/utils.js');
//var md5 = require('md5');

var User = require('./models/user.js');
var Whistle = require('./models/whistle.js');
var WhistleBlower = require('./models/whistleblower.js');
var WhistleState = require('./models/whistlestate.js');

var app = express();

/*******************/
/** Configuration **/
/*******************/
nconf.file('global', 'config/default.json');

mongoose.connect(nconf.get('dbConfig:uri'));

var port = nconf.get('port') || 9000;
var emailServer  = email.server.connect({ host: nconf.get('emailConfig:server') });
var limiter = rateLimit(nconf.get('rateLimit'));

// use handlebars as template engine
app.engine('.hbs', exphbs({
  defaultLayout: 'main',
  extname: '.hbs',
  helpers: {
    section: function(name, options){
      if (!this._sections) this._sections = {};
      this._sections[name] = options.fn(this);
      return null;
    },
    formatDate: function(date, format) {
      return moment(date).format(format);
    },
    partial: function(name) {
      return name;
    },
    replaceState: function(state) {
      return nconf.get('whistlestate:' + state);
    }
  }
}));
app.set('view engine', '.hbs');

app.enable('trust proxy', 1);

app.use(express.static('public'));
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(formidable.parse());
app.use(session({
  secret: 'fdsfdsretgregtt5y56',
  saveUninitialized: false,
  resave: false,
  cookie: nconf.get('cookie'),
  store: new MongoStore({ mongooseConnection: mongoose.connection })
}));
app.use(flash());
app.use(function(req, res, next) {
  /*var md5hash = req.session.md5hash;

  if (!md5hash) {
    md5hash = req.session.md5hash = null
  }

  if (req.body.h) {
    req.session.md5hash = req.body.h;
  }
  if (req.body.u) {
    req.session.username = req.body.u;
  }*/

  res.locals.session = req.session;
  res.locals.logo = nconf.get('config:logo');
  res.locals.logomenu = nconf.get('config:logomenu');
  res.locals.error = req.flash('error');
  next();
});
app.use(passport.initialize());
app.use(passport.session());
app.use(limiter);

/*************************/
/** Passport strategies **/
/*************************/
passport.use('whistleblower', new LocalStrategy(
  function(username, password, done) {
    User
      .findOne({'username': sanitize(username)})
      .then(function(user) {
        if (!user) return done(null, false, { message: 'Username errato' });

        if (user.isLocked) {
          return user.incLoginAttempts(function(err) {
            if (err) return done(err);

            return done(null, false, { message: 'Superato il numero di tentativi consentito' });
          });
        }

        user
          .comparePassword(password)
          .then(function(isMatch) {
            if (!isMatch) return done(null, false, { message: 'Password errata' });

            return done(null, {
              id: user._id,
              username: user.username
            });
          });
      });
  }
));

function akStrategy(usr, pwd, role) {
  return function(req, username, password, done) {
    if (username !== nconf.get(usr))
      return done(null, false, { message: 'Username errato' });

    Utils
      .comparePassword(password, nconf.get(pwd))
      .then(function(isMatch) {
        if (!isMatch) return done(null, false, { message: 'Password errata' });

        req.session.role = role;

        var obj = { id: new mongoose.Types.ObjectId() };
        obj['is' + role] = true;

        return done(null, obj);
      });
  }
}

passport.use('admin', new LocalStrategy(
  { passReqToCallback: true },
  akStrategy('admin:username', 'admin:password', 'admin')
));

passport.use('keeper', new LocalStrategy(
  { passReqToCallback: true },
  akStrategy('keeper:username', 'keeper:password', 'keeper')
));

passport.serializeUser(function(user, done) {
  return done(null, user);
});

passport.deserializeUser(function(user, done) {
  if (user.isadmin || user.iskeeper)
    return done(null, user.id);

  User.findById(user.id, function(err, user) {
    return done(err, user);
  });
});

function destroySession(req, res, redirect) {
  req.session.destroy(function() {
    res.redirect(redirect || '/login');
  });
}

function restrict (req, res, next) {
  if (selectn('session.passport.user', req)) return next();

  destroySession(req, res);
}

function isAdminAuthenticated(req, res, next) {
  if (req.isAuthenticated() && req.session.role === 'admin') return next();

  destroySession(req, res, '/loginadmin');
}

function isKeeperAuthenticated(req, res, next) {
  if (req.isAuthenticated() && req.session.role === 'keeper') return next();

  destroySession(req, res, '/loginkeeper');
}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated()) return next();

  destroySession(req, res, '/');
}

/*************/
/** Routing **/
/*************/

/***** WHISTLEBLOWER *****/
// render login page
app.get('/login', function(req, res) {
  res.render('login', {
    action: 'login',
    help: nconf.get('user:login')
  });
});

// execute login action
app.post('/login',
  passport.authenticate('whistleblower', {
    successRedirect: '/whistleblower',
    failureRedirect: '/login',
    failureFlash: true
  })
);

// show admin notes to whistleblower
app.get('/whistleblower', restrict, function(req, res) {
  var user = selectn('session.passport.user', req);

  if (!user) destroySession(req, res);

  User
    .findOne({ _id: user.id })
    .then(function(user) {
      if (!user) destroySession(req, res);

      WhistleState
        .find({ whistle: user.whistle })
        .then(function(states) {
          res.render('blower', { states: states });
          req.session.destroy();
        });
    });
});
/***** END WHISTLEBLOWER *****/

/***** ADMIN *****/
// render login page
app.get('/loginadmin', function(req, res) {
  res.render('login', {
    action: 'loginadmin',
    help: nconf.get('admin:login')
  });
});

// execute login action
app.post('/loginadmin',
  passport.authenticate('admin', {
    successRedirect: '/welcome',
    failureRedirect: '/loginadmin',
    failureFlash: true
  })
);

// execute logout
app.get('/logoutadmin', function(req, res) {
  destroySession(req, res, '/loginadmin');
});

// render welcome page
app.get('/welcome', isAdminAuthenticated, function(req, res) {
  res.render('welcome');
});

// render config page
app.get('/config', isAdminAuthenticated, function(req, res) {
  res.render('config', { config: _.omit(nconf.get('config'), ['logo']) });
});

// save logo utility
function saveLogo(logo, config) {
  if (logo.size > 0) {
    var fileName = '/img/' + logo.name;
    var source = fs.createReadStream(logo.path);
    var dest = fs.createWriteStream('public' + fileName);

    return Utils.streamToPromise(source.pipe(dest))
      .then(function() {
        nconf.set(config, fileName);
      });
  }
}

// execute config action
app.post('/config', isAdminAuthenticated, function(req, res) {
  req.body = sanitize(req.body);

  //if (req.body.pubkeywistle) fs.writeFile('public/pubkey_whistle.asc', req.body.pubkeywistle);
  //if (req.body.pubkeyuser) fs.writeFile('public/pubkey_user.asc', req.body.pubkeyuser);

  Promise.all([
    saveLogo(req.body.logo, 'config:logo'),
    saveLogo(req.body.logomenu, 'config:logomenu')
  ]).then(function() {
    var newValues = _.pick(req.body, ['email', 'presentation']);

    for (var key in newValues) {
      nconf.set('config:' + key, newValues[key]);
    }

    nconf.save(function(err) {
      nconf.load();
      res.redirect('config');
    });
  });
});

// render whistles list
app.get('/whistles', isAdminAuthenticated, function(req, res) {
  Whistle
    .find()
    .then(function(whistles) {
      Promise.each(whistles, function(whistle) {
        return WhistleState
          .find({ whistle: whistle._id })
          .then(function(states) {
            whistle.state = _.last(states).state;
          });
      })
      .then(function() {
        res.render('list', {
          whistles: whistles.reverse()
        });
      });
    });
});

// show whistle details
app.get('/whistles/:id', isAdminAuthenticated, function(req, res) {
  Whistle
    .findOne({ _id: sanitize(req.params.id) })
    .then(function(whistle) {
      WhistleState
        .find({ whistle: whistle._id })
        .then(function(states) {
          whistle.states = states;

          res.render('whistle', {
            whistle: whistle,
            states: nconf.get('whistlestate')
          });
        });
    });
});

// save whistle note
app.post('/savenote', isAdminAuthenticated, function(req, res) {
  req.body = sanitize(req.body);

  new WhistleState(req.body)
    .save()
    .then(function(data) {
      res.status(200).json(data);
    });
});

// retrieve file
app.get('/getfile/:file', isAdminAuthenticated, function(req, res) {
  Whistle.file.getbyname(sanitize(req.params.file), res);
});
/***** END ADMIN *****/

/***** KEEPER *****/
// render login page
app.get('/loginkeeper', function(req, res) {
  res.render('login', {
    action: 'loginkeeper',
    help: nconf.get('keeper:login')
  });
});

// execute login action
app.post('/loginkeeper',
  passport.authenticate('keeper', {
    successRedirect: '/userdata',
    failureRedirect: '/loginkeeper',
    failureFlash: true
  })
);

// execute logout action
app.get('/logoutkeeper', function(req, res) {
  destroySession(req, res, '/loginkeeper');
});

// render userdata page
app.get('/userdata', isKeeperAuthenticated, function(req, res) {
  res.render('userdata');
});

// retrieve user data
app.get('/getuserdata', isKeeperAuthenticated, function(req, res) {
  WhistleBlower
    .findOne({ whistle: sanitize(req.query.whistleid) })
    .then(function(data) {
      res.status(200).json(data);
    });
});
/***** END KEEPER *****/

/***** KEEPER AND ADMIN *****/
// render change password page for admins
app.get('/pwd', isAuthenticated, function(req, res) {
  res.render('pwdhash');
});

// change the password based on user role
app.post('/pwd', isAuthenticated, function(req, res) {
  var hash = req.body.pwd
  //console.log(req.session.role + ':password', hash);
  nconf.set(req.session.role + ':password', hash);
  nconf.save(function(err) {
    nconf.load();
    res.status(200).json({result: 'done'});
  });
});
/*****END KEEPER AND ADMIN *****/

/*function checkMD5(md5string) {
  return new Promise(function(resolve, reject) {
    if (md5string === md5(nconf.get('secretString') + moment(new Date()).format("YYYYMMDD"))) {
      resolve();
    } else {
      reject();
    }
  });
}*/

function save(req, res) {
  req.body = sanitize(req.body);

  new Whistle({
    msg: req.body.msg,
    files: req.body.files
  })
    .save()
    .then(function(whistle) {
      console.log('whistle saved!', whistle._id);

      new User({ whistle: whistle._id })
        .save()
        .then(function(user) {
          // send email to whistleblower
          emailServer.send({
            text: nconf.get('emailConfig:part1') +
                  'Password: ' + user.passwordclear +
                  nconf.get('emailConfig:part2'),
            from: 'whistleblowing<' + nconf.get('config:email') + '>',
            to: req.body.email,
            subject: 'whistleblowing'
          }, function(err, message) {
            console.log(err || 'Email sended to whistleblower!');
          });

          // send email to admin
          emailServer.send({
            text: nconf.get('admin:newmsg'),
            from: 'whistleblowing<' + nconf.get('config:email') + '>',
            to: nconf.get('admin:email'),
            subject: 'whistleblowing'
          }, function(err, message) {
            console.log(err || 'Email sended to admin!');
          });

          res.render('confirm', user);
        })
        .then(function() {
          new WhistleBlower({
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            whistle: whistle._id,
            user: req.body.user,
            username: req.session.username || "anonymous"
          })
            .save()
            .then(function(whistleblower) {
              console.log('whistleblower saved!', whistleblower._id);
            });
        })
        .then(function() {
          new WhistleState({
            whistle: whistle._id,
            state: 'created'
          })
            .save()
            .then(function(whistlestate) {
              console.log('WhistleState saved!', whistlestate._id);
            });
        });
    });
}

// render landing page
app.get('/', function(req, res) {
  res.render('landing', {help: nconf.get('config:presentation')});
});

/*** MD5 form landing ***/
// render whistle form page
function doSend(req, res) {
  /*checkMD5(req.session.md5hash)
    .then(function() {
      res.render('form', {
        help: nconf.get('config:form'),
        form: nconf.get('form')
      });
    }, function() {
      res.render('notauthorized');
    });*/
    res.render('form', {
        help: nconf.get('config:form'),
        form: nconf.get('form')
      });
}

app.get('/send', function (req, res){
  doSend(req, res)
});

app.post('/send', function (req, res){
  doSend(req, res)
});

// save the whistle
app.post('/save', function (req, res) {
  /*checkMD5(req.session.md5hash)
    .then(function() {
      save(req, res);
    }, function() {
      res.render('notauthorized');
    });*/
    save(req, res);
    
});
/*** end JUST for altana ***/

app.get('/keys', function(req, res) {
  res.render('createKeys');
});

app.get('/faq', function(req, res) {
  res.render('faq');
});

app.get('/technology', function(req, res) {
  res.render('technology');
});

app.get('/privacy', function(req, res) {
  res.render('privacy');
});

// handle 404 error
app.use(function(req, res, next){
  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    res.render('404', { url: req.url });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');
});

app.listen(port);
console.log('server started on port ' + port);
