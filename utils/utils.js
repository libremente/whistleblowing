'use strict';

var selectn = require('selectn');
var Promise = require('bluebird');
var crypto = require('crypto');
var bcrypt = require('bcrypt');
var nconf = require('nconf');

nconf.file('global', 'config/default.json');

var saltWorkFactor = nconf.get('encription:saltworkfactor');
var algorithm = nconf.get('encription:algorithm');
var encKey = nconf.get('encription:enckey');
var sigKey = nconf.get('encription:sigkey');

module.exports = (function() {
  Array.prototype.find = function(obj) {
    return this.filter(function(item) {
      for (var prop in obj) {
        //if (!(prop in item) || obj[prop] !== item[prop]) {
        if (obj[prop] !== selectn(prop, item)) {
           return false;
        }
      }
      return true;
    });
  };

  return {
    encKey: encKey,
    sigKey: sigKey,
    encrypt: crypto.createCipher(algorithm, encKey),
    decrypt: crypto.createDecipher(algorithm, encKey),
    streamToPromise: function(stream) {
      return new Promise(function(resolve, reject) {
        stream
          .on('close', resolve)
          .on('error', reject);
      });
    },
    hashPassword: function (password) {
      return new Promise(function(resolve, reject) {
        bcrypt.genSalt(saltWorkFactor, function(err, salt) {
          if (err) reject(err);
          // hash the password using our new salt
          bcrypt.hash(password, salt, function (err, hash) {
            if (err) reject(err);
            // set the hashed password back on our user document
            resolve(hash);
          });
        });
    	});
    },
    comparePassword: function (candidatePassword, password) {
      return new Promise(function(resolve, reject) {
        bcrypt.compare(candidatePassword, password, function(err, isMatch) {
          if (err) {
            reject(err);
          } else {
            resolve(isMatch);
          }
        });
    	});
    }
  };
})();
