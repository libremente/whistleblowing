(function userdata() {
  var crypto = new Crypto();
  var userData = null;
  var layout = document.getElementsByClassName('mdl-layout')[0];
  var modal = document.getElementById('modal');

  $('#getuserdata').submit(function (e) {
    var whistleid = $('#whistleid').val();

    $.ajax({
      url: 'getuserdata',
      data: { whistleid: whistleid }
    })
    .success(function(data) {
      try {
        userData = data;
        $('#encrypteduser').text(data.user);
        $('#decriptsection').show();
      } catch(err) {
        $('.error_msg').text('Whistle ID non presente a sistema');
        Modal.create(layout, modal);
      }
    })
    .error(function() {
      $('.error_msg').text('Errore in fase di recupero dati');
      Modal.create(layout, modal);
    });

    e.preventDefault();
    return false;
  });

  $('#decript').submit(function (e) {
    var privkey = $('#privkey').val();
    var passphrase = $('#passphrase').val();

    try {
      crypto.decryptPrivKey(privkey, passphrase);
      crypto.decryptMsg(userData.user)
        .then(function(value) {
          var user = JSON.parse(value);

          for (var p in user) {
            $('table.decrypteduser tbody')
              .append('<tr><td class="mdl-data-table__cell--non-numeric mdl-data-table__cell--wrap">'+p+'</td><td class="mdl-data-table__cell--non-numeric">'+user[p]+'</td></tr>');
          }

          $('table.decrypteduser tbody')
            .append('<tr><td class="mdl-data-table__cell--non-numeric">IP</td><td class="mdl-data-table__cell--non-numeric">'+ userData.ip +'</td></tr>')
            .append('<tr><td class="mdl-data-table__cell--non-numeric">Data</td><td class="mdl-data-table__cell--non-numeric">'+ moment(userData.date).format('DD/MM/YYYY HH:mm') +'</td></tr>')
            .append('<tr><td class="mdl-data-table__cell--non-numeric">Username</td><td class="mdl-data-table__cell--non-numeric">'+ userData.username +'</td></tr>');

          $('table.decrypteduser')
            .closest('.mdl-cell').show();

          $('.mdl-button')
            .prop('disabled', true);
        })
        .catch(function(error) {
          $('.error_msg').text('Errore in fase di decripting! Passphrase errata');
          Modal.create(layout, modal);
        });
    } catch(err) {
      $('.error_msg').text('Errore in fase di decripting! Chiave privata non valida');
      Modal.create(layout, modal);
    }

    e.preventDefault();
    return false;
  });
})();
