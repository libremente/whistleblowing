'use strict';

var mongoose = require('mongoose');
var encrypt = require('mongoose-encryption');
var crypto = require('crypto');
var bcrypt = require('bcrypt');
var Promise = require('bluebird');
var Utils = require('../utils/utils.js');

// these values can be whatever you want - we're defaulting to a
// max of 5 attempts, resulting in a 2 hour lock
var MAX_LOGIN_ATTEMPTS = 3;
var LOCK_TIME = 2 * 60 * 60 * 1000;

var UserSchema = new mongoose.Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  whistle: { type: String, required: true },
  loginAttempts: { type: Number, required: true, default: 0 },
  lockUntil: { type: Number }
});

UserSchema.virtual('isLocked').get(function() {
  // check for a future lockUntil timestamp
  return !!(this.lockUntil && this.lockUntil > Date.now());
});

UserSchema.pre('validate', function(next) {
  var user = this;

  user.username = crypto.randomBytes(16).toString('hex');
  user.password = crypto.randomBytes(16).toString('hex');
  user.passwordclear = user.password;

  //console.log('username', user.username);
  //console.log('password', user.password);

  next();
});

UserSchema.pre('save', function(next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  Utils.hashPassword(user.password)
    .then(function(hash) {
      user.password = hash;
      next();
    }, function(err) {
      next(err)
    });
});

UserSchema.methods.comparePassword = function(candidatePassword) {
  return Utils.comparePassword(candidatePassword, this.password);
};

UserSchema.methods.incLoginAttempts = function(cb) {
  // if we have a previous lock user has expired, restart at 1
  if (this.lockUntil && this.lockUntil < Date.now()) {
    return this.update({
      $set: { loginAttempts: 1 },
      $unset: { lockUntil: 1 }
    }, cb);
  }
  // otherwise we're incrementing
  var updates = { $inc: { loginAttempts: 1 } };
  // lock the account if we've reached max attempts and it's not locked already
  if (this.loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS && !this.isLocked) {
    updates.$set = { lockUntil: Date.now() + LOCK_TIME };
  }
  return this.update(updates, cb);
};

UserSchema.plugin(encrypt, {
  encryptionKey: Utils.encKey,
  signingKey: Utils.sigKey,
  decryptPostSave: false,
  excludeFromEncryption: ['username', 'loginAttempts', 'lockUntil'],
  additionalAuthenticatedFields: ['username']
});

module.exports = mongoose.model('User', UserSchema);
