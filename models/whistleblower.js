'use strict';

var mongoose = require('mongoose');
var encrypt = require('mongoose-encryption');
var Utils = require('../utils/utils.js');

var WhistleBlowerSchema = new mongoose.Schema({
  ip: { type: String, required: true },
  user: { type: String, required: true },
  date: { type: Date, default: Date.now },
  whistle: { type: String, required: true }
});

WhistleBlowerSchema.plugin(encrypt, {
  encryptionKey: Utils.encKey,
  signingKey: Utils.sigKey,
  decryptPostSave: false,
  excludeFromEncryption: ['whistle']
});

module.exports = mongoose.model('WhistleBlower', WhistleBlowerSchema);
