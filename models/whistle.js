'use strict';

var _ = require('underscore');
var mongoose = require('mongoose');
var encrypt = require('mongoose-encryption');
var selectn = require('selectn');
var crypto = require('crypto');
var bcrypt = require('bcrypt');
var Promise = require('bluebird');
var Grid = require('gridfs-stream');
var fs = require('fs');
var archiver = require('archiver');
var unzip = require('unzip');
var Utils = require('../utils/utils.js');

var gfs = null;
Grid.mongo = mongoose.mongo;
mongoose.connection.once('open', function() {
  gfs = Grid(mongoose.connection.db);
});

var WhistleSchema = new mongoose.Schema({
  msg: { type: String, required: true },
  files: { type: Array },
  date: { type: Date, default: Date.now }
});

WhistleSchema.pre('validate', function(next) {
  //if (this.files && this.files.length > 0) {
  if (!this.files) {
    this.files = [];
    return next();
  }

  var zip = archiver.create('zip', {});
  var zipname = Date.now() + '.zip';
  var writestream = gfs.createWriteStream({
    filename: zipname
  });

  Promise
    .map(_.compact(this.files), function(file) {
      zip.file(file.path, { name: file.name });
    })
    .then(function () {
      zip
        .finalize()
        .pipe(writestream);

      next();
    });

  this.files = [zipname];
});

WhistleSchema.plugin(encrypt, {
  encryptionKey: Utils.encKey,
  signingKey: Utils.sigKey,
  decryptPostSave: false,
  excludeFromEncryption: ['date']
});

var Whistle = mongoose.model('Whistle', WhistleSchema);

Whistle.file = {};

/**
 * Retrieve file from db
 */
Whistle.file.getbyname = function(file, res) {
  console.log('get', file);
  var readstream = gfs.createReadStream({filename: file});
  readstream.pipe(res);
}

module.exports = Whistle;
