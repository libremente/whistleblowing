'use strict';

var nconf = require('nconf');
nconf.file('global', 'config/default.json');
var email = require('emailjs');
var emailServer  = email.server.connect({ host: nconf.get('emailConfig:server') });

setTimeout(function() {
  emailServer.send({
    text: 'test blocco spam - ' + nconf.get('admin:newmsg'),
    from: 'whistleblowing<' + nconf.get('config:email') + '>',
    to: nconf.get('admin:email'),
    subject: 'whistleblowing'
  }, function(err, message) {
    console.log(err || 'Email sended to admin!');
  });
}, 1000);
