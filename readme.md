#Whistleblowing

Applicazione del Comune di Venezia per l'invio di segnalazioni al responsabile anti corruzione.
Tale soluzione è basata sulle indicazioni fornite dell'autorità anticorruzione.
Il software è distribuito secondo la licenza free software AGPL-3.0-or-later

http://www.anticorruzione.it/portal/public/classic/AttivitaAutorita/AttiDellAutorita/_Atto?ca=6123

L'applicazione implementa un sistema a doppia crittografia:
criptazione asimmetrica lato client (sul browser)
criptazione simmetrica lato server.

I dati inerenti al fatto, il messaggio ed i files vengono criptati (lato client) con la chiave pubblica del responsabile anti corruzione.
I dati anagrafici del delatore vengono criptati (lato client) con la chiave pubblica del responsabile unico dei sistemi informativi.
I dati vengono dunque inviati al server già criptati, rendendo inefficaci attacchi di tipo "man in the middle".
Una volta inviati al server, i dati sono ulteriormente criptati con chiave simmetrica e salvati all'interno del database.

Dopo aver acceduto all'area di sua pertinenza con username e password (/loginadmin), il responsabile anti corruzione è in grado di visualizzare la lista delle segnalazioni. Dopo aver cliccato sul bottone "visualizza" di un item della lista, sarà possibile visualizzare il contenuto della segnalazione solamente dopo aver inserito la chiave privata personale e la relativa passphrase. La decriptazione viene effettuata lato client (sul browser), rendono inefficaci attacchi di tipo "man in the middle", in quanto i dati vengono trasmessi ancora encriptati.
Il responsabile anti corruzione è in grado di inserire per ogni segnalazione, specificando lo stato di avanzamento, tutte le note che ritiene opportune. Tali note saranno visualizzabili anche dal delatore, e per questo criptate solamente in modo simmetrico lato server.

I dati anagrafici del delatore possono essere visualizzati dal responsabile unico dei sistemi informativi solo su richiesta della magistratura, ma soprattutto solo dopo aver ricevuto l'identificativo della segnalazione da parte del responsabile anti corruzione.
Il responsabile unico dei sistemi informativi, dopo aver acceduto all'area di sua pertinenza con username e password (/loginkeeper), ed aver inserito l'identificativo della segnalazione, sarà in grado di visualizzare i dati del delatore solamente dopo aver inserito la chiave privata personale e la relativa passphrase.
Anche in questo caso la decriptazione viene effettuata lato client (sul browser), rendono inefficaci attacchi di tipo "man in the middle", in quanto i dati vengono trasmessi ancora criptati.

Il delatore dopo aver inviato la segnalazione, riceverà una username ed una password(quest'ultima per email), ed accedendo tramite queste all'area a lui dedicata (/login), sarà in grado di visualizzare lo stato di avanzamento e le relative note della sua segnalazione.

La crifratura lato server è eseguita con algoritmo AES-256-CBC con vettore unico ad inizializzazione random per ogni operazione.
La crifratura lato client è implementata attraverso il protocollo OpenPGP.

Questo è il flusso delle operazioni in invio dei dati
client encription -> server encription -> database saving

Questo è invece il flusso delle operazioni in lettura dei dati
database loading -> server decription -> client decription

Le principali tecnologie utilizzate sono:
NodeJS
MongoDB
OpenPGP.js
Material design


#howto per eseguire l'applicazione

installare git

Installa nodejs (dalla 4.2 in su, si consiglia l'utilizzo di nvm)
Installa mongodb 3

clona il progetto
git clone git@xxx.xxx.xxx.xxx:whistleblowing

da riga di comando, posizionandoti all'interno della cartella del progetto

installa nodemon
npm install nodemon -g

installa le dipendenze nodejs
npm install

installa le dipendenze client
bower install

lancia l'applicazione
nodemon app.js

per testare la sicurezza delle dipendenze nodejs
nsp check

per la messa in produzione si consiglia di utilizzare pm2

Nel file config/default.json sono salvate le credenziali dei due utenti amministrativi
Le password sono salvate come hash

/loginadmin
username: admin
La password dell'utente admin e' calippo
Utils.hashPassword('calippo').then(function(hash) {
  console.log('calippo', hash);
});

/loginkeeper
username: keeper
La password dell'utente keeper e' cornetto
Utils.hashPassword('cornetto').then(function(hash) {
  console.log('cornetto', hash);
});


per visualizzare i processi node ed il loro stato eseguire

pm2 list


#database

Ogni istanza di whistleblowing ha il suo db su Mongo
Per
accedervi come admin a mongo
recuperare le credenziali di ogni istanza
creare un nuovo db
creare un nuovo utente
assegnare i permessi

fare riferimento al file dbUsers.txt


#configurazione

ogni istanza del whistleblowing è configurata all'interno del file
config/default.json


#installazione nuova istanza

accedere al server da terminale

accedere a mongo come admin
mongo admin -u username -p password --authenticationDatabase admin

creare un nuovo db, utente e ruoli
use wbtest

db.createUser(
  {
    user: "wbtest",
    pwd: "test2017",
    roles: [ { role: "readWrite", db: "wbtest" } ]
  }
)

db.grantRolesToUser("wbtest", ["readWrite", "dbAdmin"]);
aggiungere gli script al file dbUsers.txt

cambiare user linux
su nodejs

copiare wbAmes in una nuova cartella (wbTest) per la versione SENZA riconoscimento intranet MD5
copiare wbAVM in una nuova cartella (wbTest) per la versione CON riconoscimento intranet MD5

configurare l'istanza del whistleblowing all'interno del file
config/default.json

Le cose minime da modificare sono
port (siperiore alla 9010)
dbConfig.uri
config.email
config.logo
config.logomenu
admin.password
keeper.password

per la generazione delle password modificare i valori presenti all'interno del file genPassword.js
ed eseguirlo da console attraverso il comando
node genPassword.js

titoli, descrizioni, piva e cf vanno modificate nelle views
views/layouts/main.hbs
views/partials/footer.hbs

i loghi invece sono contenuti nella cartella
public/img

le chiavi pubbliche e private dei due resp possono essere generate all'url
/keys

la chiave pubblica del responsabile anti corruzione (rpc) va nel file
public/pubkey_whistle.asc

la chiave pubblica del responsabile sistemi informativi (keeper) va nel file
public/pubkey_user.asc


#esecuzione

entrare nella cartella del wbTest
cd wbTest
e lanciare la nuova istanza con il comando
pm2 start app.js --name wbTest

poi eseguire
pm2 list
per avere il numero di id e lo stato dell'istanza

alcuni comandi utili di pm2
pm2 restart <id>
pm2 stop <id>


#eliminazione dati

si accede al db con le credenziali
mongo wbtest -u wbtest -p test2017
e si fa un drop del db
use wbtest
db.runCommand( { dropDatabase: 1 } )


#client intranet

nella cartella wbTestClient vi è il codice esempio per l'autorizzazione alla compilazione del form
codice da inserire all'interno delle intranet
nel file index.php vi è la versione con MD5
funziona sia in POST che in GET

la versione MD5 usa la funzione checkMD5 in app.js del branch master
git checkout master

la versione senza autenticazione è nel branch simpleFormLanding
git checkout simpleFormLanding


#funzionalità whistleblowing

url: /loginadmin

Per accedere all'area riservata del responsabile anti corruzione servono

    username
    password

Da qui si avrà accesso al menu di configurazione ed alla lista di whistle ricevuti
Per poter leggere il contenuto di un whistle, si dovrà

    selezionale il bottone a dx della riga
    inserire chiave privata e passphrase

url: /config

Una volta autenticato rpc può configurare in autonomia alcuni parametri dell'applicazione
loghi
email rpc
testo introduttivo iniziale

url: /loginkeeper

Per accedere all'area riservata del responsabile dei sistemi informativi servono

    username
    password


per poter leggere i dati relativi ad un whistleblower bisogna

    possedere numero del whistle, comunicato dal responsabile anti corruzione
    inserire chiave privata e passphrase


url: /keys

Genera chiave privata e chiave pubblica

La chiave pubblica va comunicata a Venis SPA.
E' necessaria per far si che i messaggi siano letti solamente dalla persona in possesso della chiave privata.

La chiave privata, e relativa passphrase, vanno salvate con estrema cura.
In caso di perdita NON sarà PIU' possibile leggere le segnalazioni ricevute.
