'use strict';

var expect = require('chai').expect;
var mongoose = require('mongoose');
var nconf = require('nconf');
var Whistle = require('../models/whistle.js');

nconf.file('global', '../config/default.json');

var whistleID = null;
//var whistleID = '564f2fc39cda047848ccaa4f';

describe('Whistle', function () {

	before(function (done) {
    mongoose.connect('mongodb://localhost/whistleblowing', done);
  });

	describe('#save', function() {
		it('should save the whistle without error', function() {
			new Whistle({
		    msg: 'test',
		    files: null
		  })
			.save()
			.then(function (whistle) {
				whistleID = whistle._id;
        expect(whistle).be.an('object');
		  });
		});
	});

	describe('#findById', function() {
		it('should find whistle', function() {
			//console.log('whistleID', whistleID);
			Whistle
				.findById(whistleID, function(err, whistle) {
					console.log('whistle.to.exist', err, whistle);
		      expect(whistle).to.exist();
				});
		});
	});

	describe('#remove()', function() {
		it('should remove the whistle', function() {
			Whistle
				.findOneAndRemove({ _id: whistleID }, function (whistle) {
					//console.log('findOneAndRemove', whistle);
		      //expect(whistle).to.be(null);
					expect(whistle).to.not.be(null);
				});
		});
	});

	describe('#findById', function() {
		it('should find nothing', function() {
			//console.log('whistleID', whistleID);
			Whistle
				.findById(whistleID, function(err, whistle) {
					console.log('whistle.to.be.null', err, whistle);
					expect(whistle).to.be(null);
				});
		});
	});

});
